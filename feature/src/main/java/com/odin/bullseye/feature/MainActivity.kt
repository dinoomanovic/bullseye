package com.odin.bullseye.feature

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.androidannotations.annotations.EActivity
import java.util.*
import android.content.DialogInterface
import android.content.Intent
import java.lang.Math.abs


@EActivity
class MainActivity : AppCompatActivity(), View.OnClickListener {

    var curVal = 0
    var targetVal = 0
    var scoreVal = 0
    var roundVal = 0
    var min = 1
    var max = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        score.text = zero
        round.text = zero
        slider?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                // Write code to perform some action when progress is changed.
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // Write code to perform some action when touch is started.
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                // Write code to perform some action when touch is stopped.
                curVal = seekBar.progress
            }
        })
        button.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        startNewGame()
    }

    fun updateLabels() {
        targetLabel.text = targetVal.toString()
        score.text = scoreVal.toString()
        round.text = roundVal.toString()
    }

    fun startNewRound() {
        val r = Random()
        val randomNum = r.nextInt(max - min + 1) + min
        targetVal = randomNum
        curVal = 50
        roundVal += 1
        slider.progress = curVal
        updateLabels()
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.button -> {
                val diff = abs(curVal - targetVal)
                var points = 100 - diff
                val title: String
                        if (diff == 0) {
                            title = "Perfect!"
                            scoreVal += 100
                        } else if (diff < 5) {
                            title = "You almost had it!"
                            if (diff == 1) {
                                points += 50 }
                        } else if (diff < 10) {
                            title = "Pretty good!"
                        } else {
                            title = "Not even close..."
                        }
                scoreVal += points

                val builder1 = AlertDialog.Builder(this)
                builder1.setTitle(title)
                builder1.setMessage("You scored: " + scoreVal)
                builder1.setCancelable(false)

                builder1.setPositiveButton(
                        "Awesome",
                        object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface, id: Int) {
                                startNewRound()
                                dialog.cancel()
                            }
                        })


                val alert11 = builder1.create()
                alert11.show()
            }
            R.id.button2 -> {
                startNewGame()
            }
            R.id.button3 -> {
                val intent = Intent(this, SecondActivity::class.java)
                startActivity(intent)
                this.overridePendingTransition(R.anim.from_middle, R.anim.to_middle)
            }
            else -> {
            }
        }
    }

    fun startNewGame() {
        score.text = zero
        round.text = zero
        scoreVal = 0
        roundVal = 0
        startNewRound()
    }

}

