package com.odin.bullseye.feature

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.second_activity.*
import org.androidannotations.annotations.EActivity

@EActivity
class SecondActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.buttonClose -> {
                onBackPressed()
            }
            else -> {
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_activity)
        buttonClose.setOnClickListener(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.overridePendingTransition(R.anim.to_middle, R.anim.from_middle)
    }
}
